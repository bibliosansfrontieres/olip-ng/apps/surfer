########
# BASE #
########

FROM node:21-bookworm AS base

# Pnpm configuration
ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"
RUN corepack enable

WORKDIR /app/code

RUN curl -L https://git.cloudron.io/cloudron/surfer/-/archive/v5.18.0/surfer-v5.18.0.tar.gz -o surfer-v5.18.0.tar.gz

RUN tar -xzf surfer-v5.18.0.tar.gz --strip-components=1

RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install

RUN pnpm run build

RUN groupadd -r cloudron && useradd -r -g cloudron cloudron

COPY config/.config.json config/.users.json ./

EXPOSE 3000

CMD [ "node", "server.js"]
